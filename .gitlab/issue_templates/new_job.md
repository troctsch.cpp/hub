# New Job
<!-- Title should respect this syntax [New job] - {jobname} -->
<!-- Sample of usage of this template ➡️ https://gitlab.com/r2devops/hub/-/issues/111 -->

## Objective
<!-- Summarize concisely the objective expected by this job -->
<!-- Identify clearly the benefits will help the community to contribute on your job -->

## Use cases
<!-- Explain how the job could work -->

## Artifacts & Return status
<!--
List the artifacts expected by this job and the return status in these cases
- **When success:**
- **When failed:**

Describe how artifacts will be integrated on platform
Relevant screenshots or logs can be provided - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.
-->

## Possible stages or labels for this job
<!-- Identify Stages and Labels available by checking [the documentation](https://r2devops.io/jobs/) -->
